package am.gov.myapplication

import android.content.Context
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.LayerDrawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import kotlin.properties.Delegates

class DynamicProgressView(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs) {

    var count = DEFAULT_COUNT_VALUE
        set(value) {
            field = if (value < 1) {
                1
            } else {
                value
            }
        }

    var level = DEFAULT_LEVEL_VALUE
        set(value) {
            field = if (value < 0) {
                0
            } else {
                value
            }
            draw()
        }

    init {
        orientation = HORIZONTAL
        draw()
    }

    private fun draw() {
        removeAllViews()

        for (i in 1..count) {
            val isFill = i <= level

            when (i) {
                1 -> addView(generateView(ShapeType.LEFT, isFill))
                count -> addView(generateView(ShapeType.RIGHT, isFill))
                else -> addView(generateView(ShapeType.CENTER, isFill))
            }
        }
    }

    private fun generateView(shapeType: ShapeType, isFill: Boolean): View {
        val view = LayoutInflater.from(context).inflate(R.layout.single_view_layout, null)
        val layoutParams = LayoutParams(0, LayoutParams.WRAP_CONTENT, 1.0f)
        view.layoutParams = layoutParams

        val lineImageView = view.findViewById<ImageView>(R.id.line_image_view)
        val circleImageView = view.findViewById<ImageView>(R.id.circle_image_view)

        lineImageView.setImageDrawable(generateLineShape(shapeType, isFill))
        circleImageView.setImageDrawable(generateCircleShape(isFill))

        return view
    }

    private fun generateCircleShape(isFill: Boolean): Drawable {
        return (if (isFill) {
            ContextCompat.getDrawable(context, R.drawable.fill_cicrle_shape)
        } else {
            ContextCompat.getDrawable(context, R.drawable.empty_circle_shape)
        })!!
    }

    private fun generateLineShape(shapeType: ShapeType, isFill: Boolean): GradientDrawable {
        val shape: GradientDrawable = when (shapeType) {
            ShapeType.LEFT ->
                ContextCompat.getDrawable(context, R.drawable.line_left_rounded_shape) as GradientDrawable
            ShapeType.CENTER ->
                ContextCompat.getDrawable(context, R.drawable.line_center_shape) as GradientDrawable
            ShapeType.RIGHT ->
                ContextCompat.getDrawable(context, R.drawable.line_right_rounded_shape) as GradientDrawable
        }

        if (isFill) {
            shape.setColor(ContextCompat.getColor(context, R.color.fill_color))
        } else {
            shape.setColor(ContextCompat.getColor(context, R.color.empty_color))
        }

        return shape
    }

    companion object {
        const val DEFAULT_COUNT_VALUE = 2
        const val DEFAULT_LEVEL_VALUE = 0
    }

    enum class ShapeType {
        LEFT,
        CENTER,
        RIGHT
    }
}